# -*- coding: iso-8859-15 -*-
# Programa que apresenta a forma de uma onda progressiva
# do tipo y(x,t)=y_m*sen(k*x-w*t)

from matplotlib.pylab import *  # pylab � a maneira f�cil de fazer gr�ficos
import time                     # vamos animar os gr�ficos em tempo real
 
ion()   #Ativa o modo interativo (para a anima��o do gr�fico)

# Defini��es
L_corda=10.0 # comprimento da corda
Lambda=2.0  # comprimento de onda
freq=0.5   # frequ�ncia
 
k=2.0*pi/Lambda # n�mero de onda
w=2.0*pi*freq   # frequ�ncia angular

x = arange(0,L_corda,0.05) # cria o limite do eixo de x ao comprimento da corda
progressiva, = plot(x,x)   # Apenas cria o gr�fico inicial
progressiva.axes.set_ylim(-3,3)   # define os limites do gr�fico
 

starttime = time.time()         # O tempo inicial
t = 0                           # Quanto tempo passou

while(t < 500.0):              # while(True): para rodar para sempre

    # Calcula por quanto tempo o programa est� rondando  
    t = time.time() - starttime 

    # Calcula a forma da onda
    prog1 = 2.0*sin(k*x-w*t) 
 
    # Atualiza o gr�fico (com a soma das ondas
    progressiva.set_ydata(prog1)

    draw()                      # Redesenha a tela

    # Aguarda um instante para salvar um pouco o processador 
    time.sleep(0.03)           
