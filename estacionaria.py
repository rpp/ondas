# -*- coding: iso-8859-15 -*-
# Programa que ilustra a constru��o de uma onda estacion�ria
# a partir de duas componentes progressivas

from matplotlib.pylab import *  # pylab � a maneira f�cil de fazer gr�ficos
import time                     # vamos animar os gr�ficos em tempo real
 
ion()   #Ativa o moto interativo (para a anima��o do gr�fico)

# Defini��es
L_corda=5.0 # comprimento da corda
Lambda=2.5  # comprimento de onda
freq=0.1   # frequ�ncia
 
k=2.0*pi/Lambda # n�mero de onda
w=2.0*pi*freq   # frequ�ncia angular

x = arange(0,L_corda,0.01) # cria o limite do eixo de x ao comprimento da corda
estacionaria, = plot(x,x)   # Apenas cria o gr�fico inicial
progressiva1, = plot(x,x)
progressiva2, = plot(x,x)
estacionaria.axes.set_ylim(-3,3)   # define os limites do gr�fico
 

starttime = time.time()         # O tempo inicial
t = 0                           # Quanto tempo passou

while(t < 500.0):               # while(True): para rodar para sempre
                              
    # Calcula por quanto tempo o programa est� rondando
    t = time.time() - starttime

    # Resultado anal�tico da onda estacion�ria
    # y = 2.*sin(k*x)*cos(w*t)  

    prog1 = sin(k*x-w*t) 
    prog2 = sin(k*x+w*t) 
 
    # Atualiza o gr�fico (com a soma das ondas
    estacionaria.set_ydata(prog1+prog2)
    

    # Cada componente individual
    progressiva1.set_ydata(prog1)
    progressiva2.set_ydata(prog2)


    draw()                      # Redesenha o gr�fico
    time.sleep(0.02)
