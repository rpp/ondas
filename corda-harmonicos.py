# -*- coding: iso-8859-15 -*-
# Programa que calcula a forma de uma corda vibrando considerando 
# alguns harm�nicos

from matplotlib.pylab import *  # pylab � a maneira f�cil de fazer gr�ficos
import time                     # Vamos animar os gr�ficos em tempo real
 
ion()   # Ativa o modo interativo (para a anima��o do gr�fico)

# Defini��es
L_corda=2.5  # Comprimento da corda
Lambda=5.    # Comprimento de onda da frequencia fundamental
freq=0.1     # Frequ�ncia
                         
k=2.0*pi/Lambda  # determina o n�mero de onda
w=2.0*pi*freq    # determina a frequ�ncia angular

x = arange(0,L_corda,0.01)  # o gr�fico ter� o comprimento da corda (em x)
corda, = plot(x,x,lw=2, color="black")     # apenas cria o gr�fico inicial

# Cria o gr�fico de cada harm�nico
harmo1, = plot(x,x, color="red") 
harmo2, = plot(x,x, color="blue")
harmo3, = plot(x,x, color="orange")
harmo4, = plot(x,x, color="purple")
harmo5, = plot(x,x, color="green")

corda.axes.set_ylim(-3,3)        # Define os limites do gr�fico
 
starttime = time.time()         # O tempo inicial
t = 0                           # Quanto tempo passou
 
while(t < 130.0):            # while(True): para rodar para sempre

    # Calcula por quanto tempo o programa est� rondando
    t = time.time() - starttime 

    # Calcula os primeiros harm�nicos da corda
    # As amplitudes de cada harm�nico dependem da constru��o do instrumento
    #  e podem ser estimadas pela an�lise de Fourier do som correspondente
    h1 = 1.0*sin(k*x)*cos(w*t)  # Resultado anal�tico da onda estacion�ria
    h2 = 1.0*sin(2.*k*x)*cos(2.*w*t+.00003) # Segundo harm�nico
    h3 = 1.0*sin(3.*k*x)*cos(3.*w*t+.00004) # Terceiro
    h4 = 1.0*sin(4.*k*x)*cos(4.*w*t+.00005) # ...
    h5 = 1.0*sin(5.*k*x)*cos(5.*w*t+.00001)
 
    corda.set_ydata(h1+h2+h3+h4+h5)   # A forma da onda resultante

    # apresenta o gr�fico de cada harm�nico
    harmo1.set_ydata(h1) 
    harmo2.set_ydata(h2)
    harmo3.set_ydata(h3)
    harmo4.set_ydata(h4)
    harmo5.set_ydata(h5)

    draw()                      # Redesenha a tela

    # Aguarda um instante para salvar um pouco o processador 
    time.sleep(0.03)           

